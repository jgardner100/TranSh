#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "TranSh.h"

struct block_struct {
	char *name;
	char *output_type;
	char *lines[16];
} block[] = {
	 { "do_init","default",
		{ 
		  "function cleanup {\n"
		  ,"	echo Stopping\n"
		  ,"	rm -f $TMPENV\n"
		  ,"	echo Done\n"
		  ,"	exit 0\n"
		  ,"}\n"
		  ,"trap cleanup SIGTERM SIGINT\n"
		  ,"trap \"\" SIGHUP\n"
		  ,"TMPDIR=\"$HOME/refresh/tmp\"\n"
		  ,"TMPENV=\"$TMPDIR/%s-file.$$\"\n\n"
		  ,". $HOME/refresh/bin/funcs.sh\n"
		  ,NULL
		}
	}
	,{ "do_init","python",
		{
			"import getopt\n",
			"import sys\n"
		}
	}
	,{ "do_finish","default",
		{
		  "rm -f $TMPENV\n"
		  ,"\n"
		  ,"exit 0\n"
		  ,NULL
		}
	}
};
typedef struct block_struct BLOCK;

PUBLIC void dump_block( NODE *code, char *output_type)
{
	int i, x, found;

	debug( 0, "block code = %s\n", code->name);
	debug( 0, "block type = %s\n", output_type);
	found = 0;
	for( i = 0; i < LEN(block,BLOCK); i++)
		if( strcmp( block[i].name, code->name) == 0 &&
			strcmp( block[i].output_type, output_type) == 0)
		{
			for( x = 0; block[i].lines[x] != NULL; x++)
				fprintf( yyout, block[i].lines[x], get_prop( "name"));
			found = 1;
		}

	if( ! found)
		printf( "Can't find block %s\n", code->name);
}
