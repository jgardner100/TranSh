#!/bin/bash
#
# Test comment
#
if [ "a" -a "b" ]; then
	echo "hi there"
fi

if [ "a" != "b" ]; then
	echo "hi there"
fi

if [ ! "a" = "b" ]; then
	echo "hi there"
fi

while [ "$a" != "yes" -o "$a" = "no" ]
do
	echo "hi"
done

while read line
do
	echo $line
done
