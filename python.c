#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "TranSh.h"

extern void dump_expr( NODE *expr);
extern char *strip_str( char *str);
extern char *type_to_name( int type);

PRIVATE LISTHEAD *global_var = NULL;

/*
 * First crack at a Python code emitter for TranSh
 */
PRIVATE int g_linecnt=0;

PUBLIC void python_dump_comment( NODE *comment)
{
	debug( g_linecnt, "begin proc %s\n", "python_dump_comment");

        if( g_linecnt == 0)
                INDENTB(fprintf( yyout, "#!/usr/bin/python\n");)
        else
		if ( comment != NULL && comment->name != NULL)
                	INDENTB(fprintf( yyout, "%s\n", strip_cr( comment->name));)
        g_linecnt++;

	debug( g_linecnt, "end proc %s\n", "python_dump_comment");
}

PUBLIC void python_dump_statem( NODE *code)
{
        NODE *args;
        int i;

	debug( g_linecnt, "begin proc %s\n", "python_dump_statem");

        INDENT fprintf( yyout, "%s ", code->name);

        for( args=code->args; args != NULL; args = args->next)
        {
                if( args->type == T_REDIR)
                        fprintf( yyout, ". <<EOF%s\nEOF\n", strip_redir( args->name));
                else
                        fprintf( yyout, "%s ", args->name);
        }

	debug( g_linecnt, "end proc %s\n", "python_dump_statem");
}

PRIVATE void python_dump_args(LISTHEAD *localvars, struct node_struct *arg)
{
	NODE *ptr, *type;
	char *type_str;
	int i = 0;

	for( ptr=arg; ptr != NULL; ptr = ptr->next)
	{
		type = ptr->var_type;
		type_str = type_to_str( type);
		if( i++ != 0)
			fprintf( yyout, ",");
		fprintf( yyout, "%s %s", type_str, ptr->name);
		add_var( localvars, ptr->name, type->type);
	}
}

PUBLIC void python_do_tran( NODE *prog)
{

	NODE *ptr;
	LISTHEAD *local_var;
	int x=0;

	debug( g_linecnt, "*** begin proc %s\n", "python_do_tran");

	level = 0;
	for( ptr = prog; ptr != NULL; ptr = ptr->next)
	{
		if( ptr->type == T_PROC )
		{
			local_var = start_scope();

			debug( g_linecnt, "** got T_PROC\n", NULL);

			fprintf( yyout, "def %s(", ptr->name);
			python_dump_args( local_var, ptr->args);
			add_func_args( ptr->name, ptr->args);
			fprintf( yyout, "):\n");

			JUMPUP
			dump_code_chain( NULL, ptr->code);
			JUMPBK

			local_var = end_scope( local_var);

		} else if( ptr->type == T_EXTERN )
			add_func_args( ptr->name, ptr->args);
		else
			dump_code( NULL, ptr);
		x++;
	}

	debug( g_linecnt, "*** end proc %s\n", "python_do_tran");
}

PRIVATE void python_do_if( LISTHEAD *localvars, NODE *code)
{
	INDENT fprintf( yyout, "if ");
	if( code->test_str->type == T_STRING)
		fprintf( yyout, "%s", strip_str( code->test_str->name));
	else
	{
		/* got an expr to eval */
		dump_expr( code->test_str);
	}

	fprintf( yyout, ":\n");
	JUMPUP
	dump_code_chain( localvars, code->code);
	JUMPBK

	if( code->elifp != NULL)
		dump_elif( localvars, code->elifp);

	if( code->elsep != NULL)
	{
		INDENT fprintf( yyout, "else:\n");
		JUMPUP
		dump_code_chain( localvars, code->elsep);
		JUMPBK
	}
}

PUBLIC void python_dump_func_args( char *fname, NODE *args)
{
        if( args != NULL)
        {
                check_func_args( fname, args);
                fprintf( yyout, "%s", args->name);
		if( args->next != NULL)
                       	fprintf( yyout, ",");
                for(args=args->next; args != NULL; args = args->next)
		{
                        fprintf( yyout, "%s", args->name);
			if( args->next != NULL)
                        	fprintf( yyout, ",");
		}
        }
}

/*
 * Handle the getopt command for Python
 */
PRIVATE void python_dump_getopt( char *options)
{
	char *opt_vals[128], *opt_type[128], *opt_vars[128]; /**/
	char *val_tok;

	char *val_short, *val_long, *val_name;
	char *opt_short[128], *opt_long[128], *opt_var[128];

	char *tmp_tok[MAXOPT];
	int i, opt_count;

	options = strip_str( options);

	fprintf( yyout, "parser = optparse.OptionParser()\n");
	fprintf( yyout, "parser.set_defaults(verbose=False,show_roles=False,show_diffs=True)\n");

	opt_count = 0;
        val_tok = strtok( options, "|");
        while( val_tok != NULL && opt_count < MAXOPT)
        {
		tmp_tok[opt_count] = strdup( val_tok );
        	val_tok = strtok( NULL, "|");
		opt_count++;
	}

	for( i = 0; i < opt_count; i++)
        {
                val_short = strdup( strtok( tmp_tok[i], ","));
                val_long = strdup( strtok( NULL, ","));
                val_name = strdup( strtok( NULL, ","));

                opt_short[i] = val_short;
                opt_long[i] = val_long;
                opt_var[i] = val_name;

                val_tok = strtok( NULL, ",");
        }

        for( i = 0; i < opt_count; i++)
        {
			fprintf( yyout, "parser.add_option('-%s', '--%s', action=\"store\", type=\"int\", default=\"0\", dest=\"%s\", help=\"Max threads to use\")\n", opt_short[i],opt_long[i],opt_var[i]);
        }
	fprintf( yyout, "\n");

	fprintf( yyout, "(options, args) = parser.parse_args()\n");

        for( i = 0; i < opt_count; i++)
		free( tmp_tok[i]);
}

PRIVATE void python_dump_options_values( NODE *opt_list, char *var)
{
        fprintf( yyout, "%s == %s", var, opt_list->name);
        if( opt_list->next != NULL)
        {
        	fprintf( yyout, " or ");
                python_dump_options_values( opt_list->next, var);
        }
}


PRIVATE void python_dump_options( LISTHEAD *localvars, NODE *code, char *var)
{
        tab_level( level);
        if( code->opt_list != NULL)
                python_dump_options_values( code->opt_list, var);
        fprintf( yyout, ":\n");

        JUMPUP
        if( code->code != NULL)
                dump_code_chain( localvars, code->code);
        JUMPBK

        if( code->next != NULL)
	{
        	fprintf( yyout, "elif ");
                python_dump_options( localvars, code->next, var);
	}
}

PUBLIC void python_dump_code( LISTHEAD *localvars, NODE *code)
{
	int count;
	NODE *args;
	VARTYPE *var_ptr;

	debug( g_linecnt, "end proc %s\n", "python_dump_code");

        switch( code->type)
        {
        case T_COMMENT:
                python_dump_comment(code);
                break;
        case T_BLANK:
                fprintf( yyout, "\n");
                break;
        case T_IMPORT:
                fprintf( yyout, "import %s\n", strip_str( code->name));
                break;
        case T_GETOPT:
                python_dump_getopt( code->name);
                break;
        case T_BLOCK:
                dump_block( code, "python");
                break;
        case T_STATEM:
                python_dump_statem( code);
                fprintf( yyout, "\n");
                break;
        case T_CASE:
                if( code->options != NULL)
		{
        		fprintf( yyout, "if ");
                        python_dump_options( localvars, code->options, code->var);
		}
                break;
        case T_IF:
		python_do_if( localvars, code);
                break;
        case T_DECLARE:
                dump_decl( localvars, code);
                break;
        case T_EQUALS:
                if((var_ptr = find_var(localvars, code->name)) == NULL)
                {
                        if((var_ptr = find_var(global_var, code->name)) == NULL)
                                fprintf( stderr, "%s %d: Assign to unknown %s\n", code->file_name, code->lineno, code->name);
                }
                if( var_ptr != NULL)
                        if( (code->args->type < 100) &&
                                (var_ptr->type != code->args->type)
                        )
                                fprintf( stderr, "%s %d: %s var type mismatch = %s vs %s (%s)\n",
                                                code->file_name,
                                                code->lineno,
                                                code->name,
                                                type_to_name( var_ptr->type),
                                                type_to_name( code->args->type),
                                                code->args->name
                                );

                INDENT fprintf( yyout, "%s=", code->name);
                for( count=0, args=code->args; args != NULL; args = args->next)
                {
                        if( count != 0)
                                fprintf( yyout, " ");
                        fprintf( yyout, "%s", args->name);
                        count++;
                }
                fprintf( yyout, "\n");
                break;
	case T_FUNC:
                if( find_func( code->name) != NULL)
                {
                        INDENT fprintf( yyout, "%s(", code->name);
                        python_dump_func_args( code->name, code->func_args);
                        fprintf( yyout, ")\n");
                }
                else
                {
                        INDENT fprintf( yyout, "%s()\n", code->name);
                        fprintf( stderr, "%s %d: can't find %s()\n", code->file_name, code->lineno,code->name);
                }
                break;
	case T_FOR:
		INDENT fprintf( yyout, "%s()\n", "for statement");

		if( code->func)
		{
                	INDENT fprintf( yyout, "for %s in %s()\n", code->variable, code->func->name);
			if( code->func->func_args) {
                		INDENT fprintf( yyout, "got args!! fill in here\n");
			}
		}
		else
		{
                	INDENT fprintf( yyout, "for %s in %s\n", code->variable, code->value);
		}
                INDENT fprintf( yyout, "do\n");
                JUMPUP
                dump_code_chain( localvars, code->code);
                JUMPBK
                INDENT fprintf( yyout, "done\n");
                break;

                break;
        default:
                fprintf( yyout, "python_dump_code: unknown type %d\n", code->type);
        }

	debug( g_linecnt, "begin proc %s\n", "python_dump_code");
}
